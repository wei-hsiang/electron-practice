const { app, BrowserWindow } = require('electron/main');
const path = require('node:path')//path.joinAPI 將多個路徑段連接在一起，建立一個適用於所有平台的組合路徑字串。

require('electron-reload')(__dirname, { // 自動reload
  electron: path.join(__dirname, 'node_modules', '.bin', 'electron')
});

const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')//__dirname 指向目前正在執行的腳本的路徑（在本例中為專案的根資料夾）。
    }
  })

  win.loadFile('index.html')
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

//關閉所有視窗後退出應用程式
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') { //darwin是macOS的核心
    app.quit()
  }
})